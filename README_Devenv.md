# **DEV ENV**
![devenv flow](./devenv.png)

**See all the necessary files on the devenv branch of the repo**

## **Terraform setup...**
This job runs on Jenkins worker1.
- job: start-wordpress-dev-site

From the `devenv` branch in the project repo, `main.tf` is used to create infrastructure that makes aws;
- instance `MARJ-DEVENV`
- Route 53: maps instance `MARJ-DEVENV` to DNS: `al-website-dev`

## **MARJ-DEVENV instance**
Has ALAcademyJenkinsSuperRole IAM role attached.
This instance is used to launch the dev environment using docker-compose.

Installation of necessary packages is carried out using user_data using the script `prov.tpl`. As running docker as root causes issues, the rest of the provisioning and Terraform initialization is done through Jenkins Build Step (execute shell). It contains the following: 
```sh
terraform init
terraform apply -auto-approve #This builds the MARJ-DEVENV instance and creates a local file with the private IP for Jenkins to use.

devenv_ip=`cat /home/ec2-user/devenv_ip`
echo "sleeping for 3 minutes to initialize EC2"
sleep 180 
echo "sleep complete"

ssh -o StrictHostKeyChecking=no ec2-user@$devenv_ip "
yes | git clone https://bitbucket.org/ramana11235/final_project_wordpress.git /home/ec2-user/final_project_wordpress
sudo chown -R ec2-user:ec2-user /home/ec2-user/final_project_wordpress

cd /home/ec2-user/final_project_wordpress

git checkout devenv

# COPY AL's XML from S3 bucket: bucket hardcoded MUST be dynamic
#aws s3 cp s3://marj-bucket/ALprod.xml /home/ec2-user/ALprod.xml
aws s3 cp s3://marj-bucket/data /home/ec2-user/ --recursive
aws s3 cp s3://marj-bucket/theme/automation-logic.zip ~/final_project_wordpress/wordpress/docker/theme
sudo chown -R ec2-user:ec2-user /home/ec2-user/ALprod.xml
sudo chown -R ec2-user:ec2-user /home/ec2-user/*-cp.xml
sudo mv /home/ec2-user/* /home/ec2-user/final_project_wordpress/wordpress/docker/data

# Install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
cd /home/ec2-user/final_project_wordpress/wordpress/docker
sudo chown -R ec2-user:ec2-user *
sudo env "PATH=$PATH" docker-compose up -d
sudo chown :ec2-user db_data
sleep 30
chmod +x wp_init.sh
./wp_init.sh
"
```

It includes docker-up command:
```BASH
# Docker Up
cd $HOME/final_project_wordpress/wordpress/docker
sudo env "PATH=$PATH" docker-compose up -d
sleep 30
chmod +x wp_init.sh
./wp_init.sh
```
The sleep command is used to allow the docker container running wordpress enough time to initialised. 
`wp_init.sh` is then used to execute commands inside the wordpress container to execute wp commands to perform an automatic install of wordpress, install the AL theme, install all plugins and imports the AL site and page XML files (site content) after the wordpress image reaches its own entrypoint, adding it to the dockerfile causes a setup error (to run during setup doesn't seem to work). **WARNING: wps-hide-login plugin hides wp-admin page...** 

Additionally, within wp_init.sh all the wp cli commands are run separately, tried to put this in a code block `bash -c "commands to run"` however this was unsuccessful and produced a wp cli error asking to `--allow-root` even though `--allow-root` is present. 

The Dockerfile in `wordpress/docker/` is used to make a custom image of wordpress so that the image has wpcli and awscli commands. During an image build, the contents of the data folder is copied into the image to be used later. (this includes the theme).
If the developers are going to make updates to the theme via Wordpress, the theme must be exported and uploaded to the s3 bucket.

If docker-compose up causes errors run the following commands in the MARJ-DEVENV instance:
```BASH
sudo env "PATH=$PATH" docker-compose down
docker system prune -af
sudo rm -rf db_data/ wordpress_data/
```
By deleting the wordpress_data you delete the install of wordpress, allowing you to rerun a fresh install.

## **Route 53**
The route 53 created via terraform has the dns name: http://al-website-dev.academy.labs.automationlogic.com:8000.
login credentials http://al-website-dev.academy.labs.automationlogic.com:8000/wp-admin :
- username: admin
- password: secret123!

This is used in the automated install of Wordpress to reroute links. As the setup takes the wordpress url as a string to give to the database. This is not localhost as when you access the dns and this is setup as localhost, you are redirected to localhost instead of the instance's ip or the dns endpoint.

## **INFO**
- [wp cli commands](https://developer.wordpress.org/cli/commands/)
- [wordpress plugins site](https://wordpress.org/plugins/) : for the download links for a plugin - go to this site, search for a plugin, right-click the download button and copy the link address. To install new plugin add to wp_init.sh:
  ```BASH
  container_id=$(docker ps | grep wp | awk '{print $1}')
  cont_cmd="docker exec -it $container_id"
  # container commander
  cont_cmd="docker exec -it $(docker ps | grep wp | awk '{print $1}')"
  # Activates plugin on download
  $cont_cmd wp plugin install <download link for plugin> --activate --allow-root --path="/var/www/html" --force
  ```
- Enter the container in Terminal
```sh
docker exec -it <container id> /bin/bash
```

## Development to Production
- This job is run on Jenkins in worker1.
  - job: dev-to-prod
- It contains the following in the build step (execute shell):
```sh
#!/bin/bash

#On dev, run export.sh (/home/ec2-user/final_project_wordpress/wordpress/docker/export.sh)
devenv_ip=`cat /home/ec2-user/devenv_ip`

##added -tt for TTY
ssh -tt -o StrictHostKeyChecking=no ec2-user@$devenv_ip "
sudo systemctl start docker
sudo chmod 666 /var/run/docker.sock
chmod +x /home/ec2-user/final_project_wordpress/wordpress/docker/export.sh
/home/ec2-user/final_project_wordpress/wordpress/docker/export.sh
"

#On supernode, run init.sh #Change supernode private IP
ssh -o StrictHostKeyChecking=no ec2-user@172.31.24.73 '
rm -rf /home/ec2-user/final_project_wordpress
yes | git clone https://bitbucket.org/ramana11235/final_project_wordpress.git /home/ec2-user/final_project_wordpress
cd /home/ec2-user/final_project_wordpress
git checkout production-env 
~/final_project_wordpress/wordpress/init.sh bash
```
- Changes are made by the devs to the dev site. When these have been approved, this job is run which performs 2 steps.
- First, it runs the `wordpress/docker/export.sh` in the MARJ-DEVENV instance. This is required to complete the devenv, which exports the AL site from devenv's wordpress and only PUSH approved changes to s3 bucket (marj-bucket).
- Next, in the supernode, the `init.sh` script is run to import and update the production site with the newly implemented changes.

### Manual Steps
- Navigate to the Wordpress console (UI) 
  - http://al-website.academy.labs.automationlogic.com/wp-admin
- Click CPT UI --> Tools
- Then insert the contents of the `custom_post_types` file on this branch or `all_custom_post_type.xml` in the S3 marj-bucket into the import section and select import.
- Then navigate to Tools --> Import
- Then import the files that are in the S3 bucket in the /data folder.


## Future Work

Current Issue(s):
- Devenv and production have formatting issues due to missing plugins/content for AL site, unsure of the current issue for this.
- Some of the XML files are not imported before post-types are added and post-types must be added manually.
- Marketing involvement: as part of the CICD pipeline, once the AL dev site has been launched, marketing must receive the route53 for the site and be able to approve or disapprove of changes:
  - Very nice to have: slackbot that takes responses from marketing and runs appropriate jobs for next steps. If changes are approved, this should run a jenkins jobs that runs the `export.sh` script. If changes are disapproved, marketing should be able to make comments on the site and communication disapproval back to SPINACH!

