##  Monitoring

### The approach to monitoring is shown in the diagram below: 

![](Monitoring-set-up.png)
Grafana: http://52.16.58.81:32000/ 

### **Step one**: Install Grafana onto the EKS Server [marj-supernode]
```
$ git clone https://github.com/bibinwilson/$ kubernetes-grafana.git
$ kubectl create namespace monitoring
$ kubectl create -f grafana-datasource-config.yaml
$ kubectl create -f deployment.yaml
$ kubectl create -f service.yaml
$ kubectl describe svc -n monitoring
  ouput: grafana service, locate endpoint. This is the private IP of a worker node. Get the public IP of that worker node.

  Search PublicIP:32000
  Username and Password: admin

```
[Grafana Install Tutorial](https://devopscube.com/setup-grafana-kubernetes/)

### **Step two**: Install Prometheus as the datasource onto the EKS Server [marj-supernode]
```
$ git clone https://github.com/bibinwilson/kubernetes-prometheus
kubectl create -f clusterRole.yaml
kubectl create -f config-map.yaml
kubectl create  -f prometheus-deployment.yaml 
kubectl create -f prometheus-service.yaml --namespace=monitoring
$ kubectl describe svc -n monitoring
  ouput: prometheus service, locate endpoint. This is the private IP of a worker node. Get the public IP of that worker node.

  Search PublicIP:9090

```
[Prometheus Install Tutorial](https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/)

### **Step three**: Add prometheus as the data source onto Grafana

1. Go onto Grafana's UI
2. Click on Confguration --> DataSource on the left side
3. Click on Prometheus
4. Insert endpoint into the URL
5. Click test --> should turn green

### **Step four**: Import a dashobard onto Grafana to show the data

1. Go onto Grafana's UI
2. Click on Create --> Import 
3. Insert in Import ID
4. Select datasource as Prometheus

### **Step five**: Add Cloudwatch data to get data about the instances

1. Create a new policy which has Cloudwatch and EC2 instance
2. Create a new role with this policy
3. Create a User with this policy
4. Once creates a AWS secret key and password should be created 
5. Go onto Grafana
6. Add a new datasource
7. Click on cloudwatch
8. Add key and password
9. Add region 
10. Click test --> should turn green
11. Repeat Step four
    
[Cloudwatch Tutorial](https://medium.com/@_oleksii_/using-aws-cloudwatch-in-grafana-8294b7a2e7dd)

##  Alerts

### **Step one**: Create a Slack URL 

1. Create Workspace using this [Slack Link](https://api.slack.com/apps?new_app=1)
2. Click on Incoming Webhooks --> Activate Webhooks 
3. Click on 'Add New Webhook'
4. Click on page name
5. Copy your URL 
   
   MARJ URL: https://hooks.slack.com/services/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

### **Step two**: Create a new Dashboard without Time variables

### **Step three**: Click on panel and press edit

### **Step three**: Click on the Alerts tab
- Create an alert

-------
## Testing Monitoring Concepts
### 1. ELK Setup
### 2. Sensu Setup
### 3. Grafana Setup 

### ELK Setup
#### **Install ElasticSearch** 
```
$ sudo yum -y install https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.13.1-x86_64.rpm
$ sudo su
$ cd /etc/elasticsearch
$ nano elasticsearch.yml
    Yaml file: 
    cluster.name: marj-project
    path.data: /var/lib/elasticsearch
$ systemctl start elasticsearch
```
#### **Install Kibana** 

```
$ sudo yum install -y https://artifacts.elastic.co/downloads/kibana/kibana-7.13.1-x86_64.rpm
    Yaml file:
    server.port: 5601
    elasticsearch.hosts: ["http://localhost:9200"]
```

#### **Install MetricBeat**

```
$ curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.13.2-linux-x86_64.tar.gz
$ tar xzvf metricbeat-7.13.2-linux-x86_64.tar.gz
$ Go to the config file
    output.elasticsearch:
    hosts: ["localhost:9200"]
    username: "elastic"
    password: "hello"
    setup.kibana:
    host: "localhost:5601"

```

#### **Install Sensu**

[Sensu Install](https://docs.sensu.io/sensu-go/latest/operations/deploy-sensu/install-sensu/)

#### **Install Helm**
```
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh
```
#### **Add to Prometheus**
add to config file
- job_name: 'kube-state-metrics'
  static_configs:
    - targets: ['kube-state-metrics.kube-system.svc.cluster.local:8080']

------ 
## Conclusion

Advantages of this monitoring: 
1. All is managed on one interface
2. Alerts can easily be set up through an interface, rather than a config map

Disadvantages: 
1. If system crashes it would be difficult to rebuild. If done through config of prometheus it would be easier.

Sensu monitoring: You would set up the backend Sensu on the supernode and agents on the worker nodes. Alerts will be set up through CLI or the GUI. But I couldn't get this to work on kubernetes.

ELK set up: This would be to download elasticsearch as the datasource and Kibana as the UI. Then to set up a metricbeat as the server which collects the kubernetes data. Kibana wasnt working on the EKS cluster. Might be a better approach if it can work. 

### Things to Monitor

1. Memory usage on each worker node → alert when the memory usage is too high 
2. CPU on master and worker nodes → alert when cpu is too high
3. AL-wordpress pods → alert on when pod is down
When any instance is down
4. Monitor the wordpress website (where it’s accessed from, how many visits)


### Further Work

 https://docs.aws.amazon.com/vpc/latest/mirroring/traffic-mirror-cloudwatch.html https://grafana.com/grafana/dashboards/12193

 https://jorgedelacruz.uk/2020/04/27/looking-for-the-perfect-dashboard-influxdb-telegraf-and-grafana-part-xxiii-monitoring-wordpress-with-jetpack-restful-api/

 https://grafana.com/grafana/dashboards/13191
1. Recheck the alerting system-- maybe ask Vincent for help
2. Set an alert for when AL-wordpress pod crashes
3. Monitor the wordpress website (where it’s accessed from, how many visits) 
4. Set up multiple slack channels to correspond to alert levels 

## Continuation - Team STBS week 2

### Additions to the Monitoring section
### Alerting 
When picking up the monitoring section from handover, there was a problem with the monitoring of the CPU of nodes 1, 2 and 3, of the production env for the wordpress AL website. 

The error given was: 

```
request handler error: parse "172.31.2.106:9090": first path segment in URL cannot contain colon
```
This was noticed when looking at the test rule on the alert located on one of the nodes, on the Grafana dashboard. 

Grafana > Dashboard > EKS Monitoring > Used Memory on Node 1 > edit > Alert > Test rule

After going to 

Configuration > Data Sources > Prometheus > URL

Then changing the URL from 
```
172.31.2.106:9090
```
to 
```
HTTP://172.31.2.106:9090
```
This fixed the error. 
As when testing the rule 
again, the result is now 

```
Object
firing:true
state:"pending"
conditionEvals:"true = true"
timeMs:"2.720ms"
matches:Array[1]
0:Object
metric:"Used"
value:1245998284.8
logs:Array[3]
0:Object
message:"Condition[0]: Query"
data:Object
1:Object
message:"Condition[0]: Query Result"
data:Object
2:Object
message:"Condition[0]: Eval: true, Metric: Used, Value: 1245998284.800"
data:null

etc
```

Other parts to look at 

### Specifically mentioning a member in a channel, when an alert is set off. 

This is located in 
```
alerting > notification channels > slack channels > optional slack settings > mention users
```
Then adding in their slack ID, which you can locate on their profile from slack.

Also being able to send notification of an alert until the alert has been solved. 

This is done by sending reminders, can specify how frewuently you wish this to be, i.e. every hour. 

Also when creating an alert, you must apply the changes and then remember to save the dashboard to persist your alert rule changes. As without doing this the new changes to the alert will not get saved. 

Alerts are added and configured in the Alert tab of any dashboard graph panel, letting you build and visualize an alert using existing queries.

### Escalation of alerts

So if this first alert has been going off for say more than 10 minutes and has not been addressed, a second alert has been set up, with a longer wait time, before sending the message, which can then post to a different channel, and notify someone else on the team. 

This is done in the alerts section in a grafana panel, by changing the leave for to a longer time, increasin the pending time. This second alert can be sent to another slack channel.

### Blackbox Exporter

Another aspect was to have alerts set up to notify you if the al wordpress website went down. So this is done through the prometheus blackbox exporter, which allows blackbox probing of endpoints. So prometheus collects metrics from monitored targets by scraping metrics from HTTP endpoints on these targets.So it monitors the endpoint of the website and returns the http status code, if a 200 status code is returned then the website is okay, if not then an alert is sent, via a message to slack.

The files for the blackbox exporter can be found in the folder kubernetes-blackbox exporter. 

To enable this to integrate with prometheus you need to edit the config for prometheus, by adding:



```
- job_name: 'blackbox-exporter'
        metrics_path: /probe
        params:
          module: [http_2xx]
        static_configs:
        - targets:
          - http://prometheus.io
          - http://al-website.academy.labs.automationlogic.com
        relabel_configs:
        - source_labels: [__address__]
          target_label: __param_target
        - source_labels: [__param_target]
          target_label: instance
        - target_label: __address__
          replacement: 172.31.34.64:9115
```

After editing this, ensuring that you restart prometheus so that the config updates. 

### Tracking IPs - Future work 

1. Consideration 
When considering how to monitor the incoming IPs to our AL website, these were the aspects we took into consideration:

- Ability to Intergrate with grafana
- Ability to set up Alerts
- Ease of set up

Different possible solutions that were considered:

- Cloudwatch
- GeoIP exporter 
- Google Analytics  (Different dashboard, Not integrated with grafana and AWS, most used for marketing. Hard to set alerts and do data manipulation)

Cloudwatch was chosen method to carry on with for the reasons stated above.

2. Set up of Cloudwatch Logs 

So a VPC flow log is a feature that enabled yos to capture information about the IP traffic going to and from network interfaces in our VPC. 

The flow log data can then be published to amazon cloudwatch logs. On aws you can then perfperform queries on the data, for example to find say the the top 20 source IP addresses with the highest number of rejected requests, and then display diagrams for this.

3. To finish this work 

3.1. Intergate with Grafana

3.2. Convert our IP addresses to a geolocation - ETL

So the longitude and latitude, then using the worldmap panel plugin for grafana it would show a map with circles as data points showing where the traffic is coming from. 

## Conclude
To conclclude monitorin we have set up 

- Monitoring on basics such as CPU usage, memory used/left etc
- Blackbox exporter - monitor status code of AL wordpress
- Monitoring of SSL certificates using blackbox exporter (if we had an SSL set up)
- Meaniningfull alert messages to multiple slack channels
- Escalation of Alerts
- Collection of data for monitoring of incoming IP addresses




