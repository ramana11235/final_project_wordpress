variable "subnet_ids" {
    type = list(string)
}

variable "marj_lb_sg" {
    type = list(string)
}

variable "vpc_id" {}

variable "region" {}

variable "asg_name" {
    type = string
}