/* ------------------------------- Loadbalancer ------------------------------- */

resource "aws_lb" "marj_aws_lb" {
    name = "marj-loadbalancer"
    internal = false
    load_balancer_type = "application"
    subnets = var.subnet_ids
    security_groups = var.marj_lb_sg
    enable_deletion_protection = false

    tags = {
        Environment = "production"
    }
}

/* ---------------------------- Target Group ------------------------------- */

resource "aws_lb_target_group" "marj_lb_target_group" {
  name     = "marj-lb-tg"
  port     = 30006
  protocol = "HTTP"
  vpc_id   = var.vpc_id #### aws_vpc id
}

resource "aws_autoscaling_attachment" "alwebsite_autoscaling_attachment" {
  autoscaling_group_name = var.asg_name
  alb_target_group_arn   = aws_lb_target_group.marj_lb_target_group.arn
}

# resource "aws_lb_target_group_attachment" "marj_master_node" {
#   target_group_arn = aws_lb_target_group.marj_lb_target_group.arn
#   target_id        = var.asg_name
#   port             = 30006
# }
/* -------------------------- Loadbalancer listener -------------------------- */

resource "aws_lb_listener" "marj_front_end" {
  load_balancer_arn = aws_lb.marj_aws_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.marj_lb_target_group.arn
  }
}

/* -------------------------------- Route 53 -------------------------------- */


resource "aws_route53_record" "al_website_www_FAKE" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "al-website-FAKE"
  type    = "A"
  alias {
    name                   = aws_lb.marj_aws_lb.dns_name
    zone_id                = aws_lb.marj_aws_lb.zone_id
    evaluate_target_health = true
  }
}