variable "region" {}

variable "wp_db_name" {
    type = string
    description = "wp database name"
}

variable "wp_username" {
    type = string
    description = "database username"
}

variable "wp_password" {
    type = string
    description = "database password"
}

variable "subnetA" {
    type = string
    description = "subnet id in availability zone 1a"
}

variable "subnetB" {
    type = string
    description = "subnet id in availability zone 1b"
}

variable "subnetC" {
    type = string
    description = "subnet id in availability zone 1c"
}

variable "database_sg" {
    type = string
    description = "security groups for database"
}


variable "wp_env_tag" {
    type = string
    description = "wordpress database name"
}