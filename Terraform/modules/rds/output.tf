output "wp_rds_endpoint" {
    value = aws_db_instance.marj_database_wp.address
}

output "wp_rds_username" {
    value = aws_db_instance.marj_database_wp.username
}

output "wp_rds_password" {
    value = aws_db_instance.marj_database_wp.password
    sensitive   = true
}

output "wp_rds_arn" {
    value = aws_db_instance.marj_database_wp.arn
}