output "endpoint" {
  value = aws_eks_cluster.marj_eks_cluster.endpoint
}

output "eks_cluster_name" {
  value = aws_eks_cluster.marj_eks_cluster.name
}

output "kubeconfig_certificate_authority_data" {
  value = aws_eks_cluster.marj_eks_cluster.certificate_authority.0.data
}

output "eks_asg_name" {
 value = aws_eks_node_group.marj_eks_nodegrp.resources.0.autoscaling_groups.0.name
}

