/* -------------------------------------------------------------------------- */
/*                               Security Groups                              */
/* -------------------------------------------------------------------------- */

/* ------------------------ All traffic from home IPs ----------------------- */

resource "aws_security_group" "marj_sg_home_ips" {
  name = "marj_sg_home_ips"
  description = "Allows all traffic from BaJaMS Home IPs"
  tags = {
    "Name" = "${var.team_tag}_Home_IPs_SG"
  }
  vpc_id = var.vpc_id
  ingress {
    cidr_blocks = [ "81.152.189.117/32"]
    description = "Allows all traffic from JAMAINE"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["87.80.216.64/32"]
    description = "Allows all traffic from AYESHA"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["89.211.133.106/32", "37.210.227.157/32"]
    description = "Allows all traffic from MOHAMED"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }        
  ingress {
    cidr_blocks = ["185.59.124.94/32"] # Change this everyday
    description = "Allows all traffic from RAMANA"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }    
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks = var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
}

/* ----------------------------- Loadbalancer SG ---------------------------- */

resource "aws_security_group" "marj_sg_loadbalancer" {
  name = "marj_sg_loadbalancer"
  description = "Allows port 80 all traffic and SSH from bastion"
  vpc_id = var.vpc_id
  ingress {
    from_port = var.http_port
    to_port = var.http_port
    protocol = "tcp"
    cidr_blocks = var.open_internet
    description = "Allows port 80"
    }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
    }

  egress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    cidr_blocks = var.open_internet
    ipv6_cidr_blocks = var.ipv6_cidr_block
  }
  tags = {
    "Name" = "${var.team_tag}_Loadbalancer_SG"
  }
} 

/* --------------------------- RDS Security Group --------------------------- */

resource "aws_security_group" "marj_db_sg" {
  name = "marj_db_sg"
  description = "Allows port 3306"
  vpc_id = var.vpc_id
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    from_port = var.mysql_port
    to_port = var.mysql_port
    protocol = "tcp"
    security_groups = [aws_security_group.marj_sg_home_ips.id, var.default_user_ips]
    description = "Allows port 3306 from our Home IPs"
  }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows port 3306 from local VPC"
    from_port = var.mysql_port
    protocol = "tcp"
    to_port = var.mysql_port
  }
  # ingress {
  #   security_groups = [aws_security_group.kubernetes_sg.id]
  #   description = "Allows port 3306 from kubernetes cluster"
  #   from_port = var.mysql_port
  #   protocol = "tcp"
  #   to_port = var.mysql_port
  # }

  egress {
    from_port = var.outbound_port
    to_port = var.outbound_port
    protocol = "-1"
    cidr_blocks = var.open_internet
    ipv6_cidr_blocks = var.ipv6_cidr_block
  }
  tags = {
    "Name" = "${var.team_tag}_Database_SG"
  }
} 

/* ------------------------------ Bitbucket SG ------------------------------ */

resource "aws_security_group" "marj_bitbucket" {
  name = "marj_sg_bitbucket"
  tags = {
    "Name" = "${var.team_tag}_Bitbucket_SG"
  }
  vpc_id = var.vpc_id
  description = "Allows Bitbucket Webhook to run"
  ingress {
    cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
    description = "Bitbucket IPs:80"
    from_port = 80
    protocol = "tcp"
    to_port = 80
  }
  ingress {
    cidr_blocks = ["172.31.0.0/16"]
    description = "Allows all traffic from local VPC"
    from_port = var.outbound_port
    protocol = "-1"
    to_port = var.outbound_port
  }
  ingress {
    cidr_blocks = ["13.52.5.96/28","13.236.8.224/28","18.136.214.96/28","18.184.99.224/28","18.234.32.224/28","18.246.31.224/28","52.215.192.224/28","104.192.137.240/28","104.192.138.240/28","104.192.140.240/28","104.192.142.240/28","104.192.143.240/28","185.166.143.240/28","185.166.142.240/28"]
    description = "Bitbucket IPs:8080"
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
  }
  egress {
    cidr_blocks = var.open_internet
    from_port = var.outbound_port
    ipv6_cidr_blocks = var.ipv6_cidr_block
    protocol = "-1"
    to_port = var.outbound_port
  }
}