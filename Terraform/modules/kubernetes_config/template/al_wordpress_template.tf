resource "kubernetes_namespace" "al_wordpress" {
  metadata {
    name = "al-wordpress"
  }
}

resource "kubernetes_deployment" "al_wordpress" {
  metadata {
    name      = "al-wordpress"
    namespace = "al-wordpress"

    labels = {
      app = "al-wordpress"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "al-wordpress"
      }
    }

    template {
      metadata {
        labels = {
          app = "al-wordpress"
        }

        annotations = {
          "prometheus.io/path" = "/wp-json/metrics"

          "prometheus.io/port" = "80"

          "prometheus.io/scrape" = "true"
        }
      }

      spec {
        container {
          name  = "al-wordpress"
          image = "ratstardocker/al_wordpress:REPLACE"

          port {
            container_port = 80
          }

          env {
            name  = "WORDPRESS_DB_HOST"
            value = "marj-db-wp.academy.labs.automationlogic.com"
          }

          env {
            name  = "WORDPRESS_DB_USER"
            value = "admin"
          }

          env {
            name  = "WORDPRESS_DB_PASSWORD"
            value = "finalproject!"
          }

          env {
            name  = "WORDPRESS_DB_NAME"
            value = "al_wordpress"
          }

          resources {
            requests = {
              cpu    = "200m"
              memory = "200Mi"
            }
          }

          liveness_probe {
            http_get {
              path = "/"
              port = "80"
            }

            initial_delay_seconds = 100
            timeout_seconds       = 10
            period_seconds        = 15
            failure_threshold     = 5
          }

          readiness_probe {
            http_get {
              path = "/"
              port = "80"
            }

            initial_delay_seconds = 100
            timeout_seconds       = 15
            period_seconds        = 15
            failure_threshold     = 5
          }

          lifecycle {
            post_start {
              exec {
                command = ["/bin/bash", "-c", " echo yo, waiting for db; sleep 10; echo overwriting wp-config.php; mv /tmp/wp-config.php /var/www/html/wp-config.php;\necho overwriting wp-config.php; mv /tmp/wp-config.php /var/www/html/wp-config.php;\necho installing wp; if ! wp core is-installed --allow-root; then\nwp core install --path=\"/var/www/html\" --url=\"al-website.academy.labs.automationlogic.com\" --title=\"Automatic Logic\" --admin_user=admin --admin_password=secret --admin_email=foo@bar.com --allow-root;\nfi;\necho installing dependencies; wp plugin install https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.6.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/contact-form-7.5.4.1.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/page-list.5.2.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/search-filter.1.2.14.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/tablepress.1.13.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/wp-video-lightbox.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/wp-pagenavi.2.94.0.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/classic-editor.1.6.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/wordpress-seo.16.5.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/duplicate-post.4.1.2.zip --activate --allow-root --path=\"/var/www/html\" --force;\n## wp plugin install https://downloads.wordpress.org/plugin/wps-hide-login.1.8.5.zip --activate --allow-root --path=\"/var/www/html\" --force;\n## Deactivated wordfence it was buggin with wp plugin install https://downloads.wordpress.org/plugin/wordfence.7.5.4.zip --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/syntaxhighlighter.3.6.0.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/sucuri-scanner.1.8.26.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/sg-cachepress.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/code-prettify.zip --activate --allow-root --path=\"/var/www/html\" --force;\nwp plugin install https://downloads.wordpress.org/plugin/contact-form-7-simple-recaptcha.0.0.8.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/custom-post-type-ui.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/display-php-version.1.7.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/wp-clone-template.2.2.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/uk-cookie-consent.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/jetpack.9.8.1.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/post-types-order.1.9.5.6.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/redirection.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install https://downloads.wordpress.org/plugin/leadin.zip --activate --allow-root --path=\"/var/www/html\";\nwp plugin install wordpress-importer --activate --path=\"/var/www/html\" --allow-root --force;\necho installing theme; wp theme install /tmp/automation-logic.zip --path=\"/var/www/html\" --allow-root --force;\necho activating theme; wp theme activate automation-logic --path=\"/var/www/html\" --allow-root;\n# echo importing xml; # wp import /tmp/automationlogic.WordPress.2021-06-11.xml --authors=create --path=\"/var/www/html\" --allow-root;\necho setup finished"]
              }
            }
          }

          image_pull_policy = "Always"
        }
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_unavailable = "2"
        max_surge       = "2"
      }
    }

    revision_history_limit = 100
  }
}

resource "kubernetes_service" "al_wordpress_clip_svc" {
  metadata {
    name      = "al-wordpress-clip-svc"
    namespace = "al-wordpress"

    labels = {
      app = "al-wordpress"
    }
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 80
      target_port = "80"
    }

    selector = {
      app = "al-wordpress"
    }

    type = "LoadBalancer"
  }
}

locals {
  lb_name = split("-", split(".", kubernetes_service.al_wordpress_clip_svc.status.0.load_balancer.0.ingress.0.hostname).0).0
}

data "aws_elb" "al_wordpress_clip_svc" {
  name = local.lb_name
}

# data "kubernetes_service" "service_data" {
#   metadata {
#     name = "al-wordpress-clip-svc"
#   }
#   depends_on = [
#     kubernetes_service.al_wordpress_clip_svc
#   ]
# }

# resource "local_file" "output" {
#   content = "${data.kubernetes_service.service_data.status}"
#   filename = "/home/ec2-user/test.txt"
# }

resource "aws_route53_record" "al_website_www" {
  zone_id = "Z03386713MCJ0LHJCA6DL" # AL academy 
  name    = "al-website"
  type    = "A"
  depends_on = [
    kubernetes_service.al_wordpress_clip_svc
  ]
  alias {
    name                   = data.aws_elb.al_wordpress_clip_svc.dns_name
    zone_id                = data.aws_elb.al_wordpress_clip_svc.zone_id
    evaluate_target_health = true
  }  
}

resource "kubernetes_service" "al_wordpress_db_svc" {
  metadata {
    name      = "al-wordpress-db-svc"
    namespace = "al-wordpress"

    labels = {
      app = "al-wordpress"
    }
  }

  spec {
    type          = "ExternalName"
    external_name = "al-website.academy.labs.automationlogic.com"
  }
}

# resource "kubernetes_ingress" "wordpress_ingress" {
#   metadata {
#       name = "al-website-ingress"
#       namespace = "al-wordpress"
#   }

#   spec {
#       rule {
#         host = "al-website.academy.labs.automationlogic.com"
#         http {
#           path {
#             path = "/*"
#             backend {
#               service_name = "wordpress-svc"
#               service_port = 80  
#             }  
#           }  
#         }  

#       }

#   }  
  
# }

