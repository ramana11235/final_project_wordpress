variable "cluster_name" {
  type = string
}

variable "rds_endpoint" {
  type = string
}

variable "wp_schema_name" {
    type = string
    description = "wp database name"
}

variable "wp_username" {
    type = string
    description = "database username"
}

variable "wp_password" {
    type = string
    description = "database password"
}