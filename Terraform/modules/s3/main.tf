/* -------------------------------------------------------------------------- */
/*                                  S3 Bucket                                 */
/* -------------------------------------------------------------------------- */

resource "aws_s3_bucket" "default" {
  bucket = "marj-bucket"
  acl    = "private"
  force_destroy = true
  versioning {
    enabled = true
  }
  tags = {
    Name        = "marj-bucket"
  }
}

/* -------------------------------------------------------------------------- */
/*                 Create Policy to Allow Access From Home IPs                */
/* -------------------------------------------------------------------------- */
resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.default.id
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "S3PolicyId45"
    Statement = [
      {
        Sid       = "IPAllow"
        Effect    = "Allow"
        Principal = "*"
        Action    = "s3:*"
        Resource = [ aws_s3_bucket.default.arn,
          "${aws_s3_bucket.default.arn}/*"]
        Condition = {
          IpAddress = {
            "aws:SourceIp" = [
                        "37.210.227.157/32",
                        "185.59.124.94/32",
                        "81.152.189.117/32",
                        "87.80.216.64/32"
                    ]
          }
        }
      },
    ]
  })
}





/* ----------------------------- Bajams Key pem ----------------------------- */

# resource "aws_s3_bucket_object" "object" {
#   bucket = aws_s3_bucket.default.id
#   key    = "BajamsKey.pem"
#   acl    = "private"  # or can be "public-read"
#   source = "/home/ec2-user/.ssh/BajamsKey.pem"
# }

# resource "local_file" "s3_bucket" {
#     content = aws_s3_bucket.default.id
#     filename = "/home/ec2-user/s3_bucket_id"
# }