variable "region" {
    default = "eu-west-1"
}

variable "key_name" {
    default = "marjkey"
}

variable "wp_rds_username" {
    default = "admin"
}

variable "wp_rds_password" {
    default = "finalproject!"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "instance_type_input" {
    default = "t3a.xlarge"
}

variable "subnet_1c" {
    default = "subnet-7bddfc1d"
}

variable "subnet_1a" {
    default = "subnet-a94474e1"
}

variable "subnet_1b" {
    default = "subnet-953a58cf"
}

variable "vpc_id" {
    default = "vpc-4bb64132"
}

variable "wp_env_tag" {
    default = "marj-website"
}

variable "team_name_tag" {
    default = "marj"
}


variable "wp_database_name" {
    default = "marj-database-wp"
}

variable "wp_schema_name" {
    default = "wordpress"
}