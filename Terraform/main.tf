# /* ---------------------- create/modify security groups --------------------- */
# bitbucket (webhook), home ips (dev ips)
# database (db)

module "security_groups" {
    source = ".//modules/security_groups"
    vpc_id = var.vpc_id
    region = var.region
    default_user_ips = "sg-091c83d3ecee7301e" #userhomeips
    team_tag = var.team_name_tag
}

# /* ---------------------------- create/modify RDS --------------------------- */
#  db instance, db subnet group, localfiles: dbhost, dbpass, dbuser [rds endpoint info]

module "rds_database" {
    source = ".//modules/rds"
    wp_username = var.wp_rds_username
    wp_password = var.wp_rds_password
    wp_db_name = var.wp_database_name
    subnetA = var.subnet_1c
    subnetB = var.subnet_1b
    subnetC = var.subnet_1a
    database_sg = module.security_groups.database_sg
    wp_env_tag = var.wp_env_tag
    region = var.region    
}

/* ---------------------------- create/modify EKS cluster --------------------------- */
# AWS EKS cluster manager and node setup, iam roles and cluster SG and intergrated auto scaling

module "marj_cluster" {
    source = ".//modules/eks_cluster"
    subnets = ["${var.subnet_1a}", "${var.subnet_1b}", "${var.subnet_1c}"]
    instance_type = ["${var.instance_type_input}"]
    region = var.region
}

module "kubernetes" {
    source = ".//modules/kubernetes_config"
    cluster_name = module.marj_cluster.eks_cluster_name
    rds_endpoint = module.rds_database.wp_rds_endpoint
    wp_username = var.wp_rds_username 
    wp_password = var.wp_rds_password
    wp_schema_name = var.wp_schema_name
}

resource "aws_autoscaling_policy" "eks-scale" {
 name                   = "eks-scale"
 policy_type            = "TargetTrackingScaling"
 autoscaling_group_name = module.marj_cluster.eks_asg_name

 target_tracking_configuration {
   predefined_metric_specification {
     predefined_metric_type = "ASGAverageCPUUtilization"
   }

 target_value = 80.0
 }
}

/* ---------------------------- create/modify s3 bucket --------------------------- */
# s3 bucket and key pem object

module "s3" {
    source = ".//modules/s3"
    team_name_tag = var.team_name_tag
    region = var.region
}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "marj-bucket"
    key            = "global/s3/terraform.tfstate"
    region         = "eu-west-1"
  }
}

# /* ---------------------------- create/modify loadbalancer --------------------------- */
# # -------------------------------THIS IS USELESS---------------------------------------

module "loadbalancer" {
    source = ".//modules/loadbalancer"
    vpc_id = var.vpc_id
    subnet_ids = ["${var.subnet_1a}", "${var.subnet_1b}", "${var.subnet_1c}"]
    marj_lb_sg = [module.security_groups.loadbalancer_sg]
    region = var.region
    asg_name = module.marj_cluster.eks_asg_name
}
