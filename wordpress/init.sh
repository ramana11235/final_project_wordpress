#!/bin/bash
cd ~/final_project_wordpress/wordpress/docker


# aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 242392447408.dkr.ecr.eu-west-1.amazonaws.com
# docker build -t al_wordpress .
# docker tag al_wordpress:latest 242392447408.dkr.ecr.eu-west-1.amazonaws.com/al-wordpress:latest
# docker push 242392447408.dkr.ecr.eu-west-1.amazonaws.com/al-wordpress:latest
# docker pull 242392447408.dkr.ecr.eu-west-1.amazonaws.com/al-wordpress:latest

# install docker and docker-compose
sudo yum install -y docker
sudo systemctl start docker

sudo yum install -y mariadb-server 

sudo docker login -u ratstardocker -p automationlogic

VERSION=$(date +%s)

# Stable Build
# VERSION=1623841160


aws s3 cp s3://marj-bucket/ALprod.xml ~/final_project_wordpress/wordpress/docker/data/ALprod.xml

sudo docker build -t al_wordpress:$VERSION .
sudo docker tag al_wordpress:$VERSION ratstardocker/al_wordpress:$VERSION
sudo docker push ratstardocker/al_wordpress:$VERSION
sudo docker pull ratstardocker/al_wordpress:$VERSION

rm ~/final_project_wordpress/wordpress/docker/data/ALprod.xml


cd ~/final_project_wordpress/

sed -e "s,REPLACE,$VERSION," Terraform/modules/kubernetes_config/template/al_wordpress_template.tf >Terraform/modules/kubernetes_config/al_wordpress.tf

mysql -h marj-db-wp.academy.labs.automationlogic.com -u admin -pfinalproject! --execute="DROP DATABASE al_wordpress;"
mysql -h marj-db-wp.academy.labs.automationlogic.com -u admin -pfinalproject! --execute="CREATE DATABASE IF NOT EXISTS al_wordpress;"

cd ~/final_project_wordpress/Terraform
terraform init
terraform plan
terraform apply --auto-approve 

# kubectl apply -f kubernetes/

kubectl autoscale deployment al-wordpress  -n al-wordpress --cpu-percent=50 --min=1 --max=2

echo script finished

