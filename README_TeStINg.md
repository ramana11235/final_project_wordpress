# **"Testing": branch - dev-jamaine**
Ite... I'm very sorry for this, but testing COLOURS is the purpose of this branch.

GET READY FOR A WHIRLWIND OF EMOTIONS...

![](https://media0.giphy.com/media/e1s8C0YnnfjlRf7mEr/200.gif)

---
## **CSScolourfinder**
AL:
al-grey: #313130 (contact and Automation Logic Title)
al-orange: #ED8B00 (orange text and text boxes)
al-alt-orange: #ef8c02 (carousel images)

Usability: 9/10, Necessity: 4/10.

This folder contains a python script that is the basis for colour testing. ITS WHOLE PURPOSE is to find all the colours present in the MainCSS that is used in AL's website but due to the nature of WORDPRESS... its not this simple to be implemented. 
The python script `colourfinder.py` uses regular expressions to find instances of `color` in the css script (sourced by viewing the page source of AL site | copy into browser: `view-source:https://automationlogic.com/`)

After finding all the colours, a list is compiled and the python scripts just currently just checks in AL orange ( #ed8b00 ≈Tangerine | [If you know you know](https://veli.ee/colorpedia/)) is present in the list of colours. Future work would included checking whether AL orange is in the style for classes which are defined with 'orange', for example:
```css
.orange-text a{color:#ed8b00;font-size:14px;font-weight:600;letter-spacing:.2px;text-decoration:none}
```
The main issue with this, however, is that all styling is done in the automation logic theme so testing has to be done on the theme.zip.
---
## **HTMLviewer**
Usability: 6/10, Necessity: 1/10.

**IGNORE THIS FOLDER**. This was testing an idea to view the html alongside the production website, but AL's site is managed via Wordpress so this is irrelevant. However, if you wish to see this locally, you must install the plugin `LIVE SERVER` for vscode. With this plugin, right click on testview.html and `Open with Live Server`.

---
## **Selenium**
Usability: 4/10, Necessity: 2/10.

**IGNORE THIS FOLDER unless you can get a browser on an EC2 instance**. The `tutorial.py` file is used to run selenium locally. The prerequisites for selenium are defined in `prereq.sh`. Due to time restrictions, this wasn't fleshed out. Currently, the selenium script opens the automationlogic website is an automated browser and scrapes all the 'href' links in 'link' html tags that could refer the stylesheets (css). A youtube link is present in `Selenium/tutorial.py`.
