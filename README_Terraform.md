# Production Environment
Terraform is used to set up the production environment. This was originally done on the `dev-mohamed-terraform` branch, and then integrated with the `dev-ramana` branch to include the code used for setting up the website (i.e. XML, Dockerfile, Kubernetes YAML). 

This integration was done on a new `production-env` branch which contains the most up to date code on Terraform and Wordpress. 
## Supernode 
To begin, an EC2 instance called marj-supernode was set up manually to act as a controller in which the terraform script was run. Therefore we had to install the following dependencies:

- Terraform -> to run terraform script. 
- Kubectl -> to access the EKS cluster.
- Git -> to clone in the repository.
- Docker -> to run the Dockerfile.
- Mariadb-server -> to access the RDS.

### Installing dependencies
- Clone the repository into the supernode after installing git.
```
$ sudo yum install git -y 
$ git clone https://mnasr6263@bitbucket.org/ramana11235/final_project_wordpress.git
```
- Install Terraform
```
$ wget https://releases.hashicorp.com/terraform/1.0.0/terraform_1.0.0_linux_amd64.zip
$ unzip terraform_1.0.0_linux_amd64.zip
$ ./terraform
$ sudo mv terraform /usr/local/bin
$ terraform --version
```
- Install kubectl.
```

$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
$ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
$ kubectl version --client
```
- Docker and mariadb-server are installed in the init.sh script located in the wordpress directory. 

## Infrastructure Setup
To set up the infrastructure, run the init.sh script:
```
$ cd ~/final_project_wordpress/wordpress
$ git checkout production-env
$ ./init.sh 
```
This builds the wordpress image from the Dockerfile and runs terraform commands to create the AWS infrastructure. After a successful build, you should be able to access our most recent version of the AL website through its route53 at http://al-website.academy.labs.automationlogic.com/.

We had originally used the yaml files found in the wordpress directory to apply the kubernetes resources required for the website. However we are now using terraform resources for this after we found a tool on github that can convert kubernetes yaml files into terraform scripts.

https://github.com/sl1pm4t/k2tf

For example, to create the metrics.tf file in the kubernetes_config module the following command was run:

    $ k2tf -f wordpress/kubernetes/metric-server.yml -o Terraform/modules/kubernetes_config/metrics.tf


### EKS Access
After a successful `terraform apply` command has set up the infrastructure run the following command which will give you access to the EKS cluster and allow you to use kubectl commands:

```
$ aws eks update-kubeconfig --region eu-west-1 --name marj_cluster
```
You can now run kubectl commands to retrieve information from the cluster such as:

    $ kubectl get namespace

The terraform script has been split up into modules for a more structured and easy to read layout. An EKS Cluster is set up in the module named `eks_cluster` with a node group consisting of three EC2 instances. 

## Terraform 
The terraform scripts are split up into the following modules:
- eks_cluster
- kubernetes_config
- loadbalancer
- rds
- s3 
- security_groups

A basic description of each module is found below:

### eks_cluster 
In this module an eks cluster is set up as well as a node group of at least 3 worker nodes. Two IAM roles are created for each resource, making sure to provide EKS full access as an inline policy. 

Further policies are attached as the eks cluster and node group resources create EC2 instances and an autoscaling group associated to them.

### kubernetes_config
A kubernetes provider is enabled which allows for the management of kubernetes resources. The kubeconfig.tpl file specifies the marj_cluster that we created. 

We can now create kubernetes resources using terraform as can be seen in the module. We used the k2tf tool described earlier to convert the yaml files in the wordpress/kubernetes directory into .tf files such as haproxy.tf, al_wordpress.tf and metrics.tf.   

Specifying the kubernetes service as a LoadBalancer triggers the EKS cluster to create an elastic loadbalancer on AWS. We then used a data source to retrieve data from this loadbalancer and used its dns name to attach it to a route53 record.

### loadbalancer (not needed)
This module was initially used to create an AWS application loadbalancer and attach it to the EKS autoscaling group that is created when launching the cluster. However, we started to face networking issues when trying to access the website with frequent 504 errors popping up. 

To change this we instead defined the kubernetes service as a LoadBalancer rather than ClusterIP. When a loadbalancer service is applied in an EKS cluster, an AWS elastic loadbalancer is created as described earlier. This seemed to solve the networking issues.

This loadbalancer module is basically useless but when we try to delete it we get some errors on terraform. If a solution isnt found, we could run a terraform destroy and then remove the module before running a terraform apply again.


### rds
A MySQL database was configured on an RDS and linked to a Route53 record. A database called al_wordpress is created on the RDS and the kubernetes deployment is linked to this. 

- Username: admin
- Password: finalproject!
- Route53: marj-db-wp.academy.labs.automationlogic.com  


### s3
An S3 bucket called marj-bucket is created to store the tf state everytime you run a terraform plan. This can be seen in the global directory in the S3 bucket.

The S3 bucket is also used to store the XML file for the website. Whenever a developer makes changes to the XML, they then push these changes to the S3 bucket which updates the ALprod.xml file. The init.sh script should then be executed so that a new docker image can be built with the new xml data and replaced in the kubernetes deployment resource. 

### security_groups
Four security groups are created in this module. For now, only the RDS and Home IPs security groups are being used properly. You should add your home IPs to them so that you can access the RDS through the mysql workbench. 

The loadbalancer security group can be ignored as well. 

## Future Work
- Git rid of the loadbalancer module and security group without causing Terraform errors. 
- Update the kubernetes deployment resource in al-wordpress.tf with all the required plugins so that the website functions correctly.
- Fix liveness and readiness probes in deployment file, as there is a problem with the `path`. 
### Jenkins
- A job that sets up the monitoring services on the supernode after the infrastructure has been set up
- A job which runs the init.sh script when a change is approved from the dev environment so that the new XML can be used.