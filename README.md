- [Deliverables](#deliverables)
- [Our Plan](#our-plan)
  - [The Problem](#the-problem)
- [Potential Pipeline/Workflow of AL Wordpress from Development to Production](#potential-pipelineworkflow-of-al-wordpress-from-development-to-production)
  - [Pipeline Translated to Jenkins Job](#pipeline-translated-to-jenkins-job)
  - [What We Know About How Wordpress Works and What That Means for the Project](#what-we-know-about-how-wordpress-works-and-what-that-means-for-the-project)
    - [WP-CLI To Automate Wordpress](#wp-cli-to-automate-wordpress)
    - [WP-CLI Commands and Options We've Used](#wp-cli-commands-and-options-weve-used)
      - [wp core install](#wp-core-install)
      - [--allow-root](#--allow-root)
      - [wp plugin install](#wp-plugin-install)
      - [wp theme install and wp theme activate](#wp-theme-install-and-wp-theme-activate)
      - [wp import /tmp/ALProd.xml](#wp-import-tmpalprodxml)
    - [Importing AL's Wordpress Content, Themes, and Plugins](#importing-als-wordpress-content-themes-and-plugins)
  - [Setting Up Wordpress](#setting-up-wordpress)
    - [Known Problems and How To Reproduce Them](#known-problems-and-how-to-reproduce-them)
      - [Missing Plugins](#missing-plugins)
        - [Link to Wordpress Plugins List](#link-to-wordpress-plugins-list)
      - [When Exporting XML from Dev](#when-exporting-xml-from-dev)
  - [Development Environment](#development-environment)
  - [Production Environment](#production-environment)
- [Wordpress Testing CSS/HTML](#wordpress-testing-csshtml)
- [Metrics Monitoring and Notifications](#metrics-monitoring-and-notifications)
- [Why we chose the technology we're using](#why-we-chose-the-technology-were-using)
  - [Why Production on Kubernetes and Development on Docker?](#why-production-on-kubernetes-and-development-on-docker)
  - [Business Considerations](#business-considerations)
- [Rollback Deployment for Version Control](#rollback-deployment-for-version-control)
- [Commands Shortcut](#commands-shortcut)
- [Things To Do Next](#things-to-do-next)






## Deliverables 

- In terms of providing a business case as to why we should do this, we will NOT be providing a case as to why AL should host their website internally
  - This is because this has already been discussed and agreed upon prior to the start of the project
  - In our presentation we can provide the business case and the pain points as a background/context
  - Define clear boundaries of what we can and cannot deliver in terms of business value
    - At the end provide an estimate of how much running the infrastructure costs


- Specify all the tools we use and provide reasons as to why we use each of the tool we use 

- Planning for 2 weeks of work, knowing hand off in 1
  - Design a clear plan showing what is expected to be complete by when


## Our Plan

### The Problem

The context of this problem is that the AL website is hosted by a third party and if AL wants any changes made to the website (e.g. new blog post, changing themes, etc.) they ask the third party to do so. The site the third party hosts is the final, production site, this means that if any of the changes have any errors (spelling mistakes, inconsistent formatting, etc.) it gets pushed onto the live production with no checking in between.   

Another problem is that they have no monitoring enabled on the website. If it gets breached, there is no way of knowing unless someone goes on the website and checks for themselves.

Our solutions to the main key problems are these:


1. Create a development environment of WordPress which then becomes the only AL site that the third party has access too to make any changes. This development website would also be viewable by people from Marketing and other relavant dons.


2. Create and host our own production-ready AL website. Any changes/updates to this would have been pre-approved by Marketing/relevant persons by viewing the development environment website and checking for errors. 


3. Automate the testing of the CSS/HTML to catch any inconsitencies in the formatting (how the hell do we do this)

4. Enable metrics and alerts on instances/Kubernetes pods in order to enable monitoring

We'll explain what we've done in each of the sections below but first we need to explain the potential pipeline of Wordpress Development to Production.

## Potential Pipeline/Workflow of AL Wordpress from Development to Production

If AL want a change in their website, they currently ask the third party to do that. As the third party have control of the production website, we want to sever that and give them access to only the AL Dev website. This means that the AL production website is fully in AL's hands and it also means that changes can be approved in the Development environment to Production.

We're planning to implement this by creating our down development environment that is essentially an EC2 instance that runs a Wordpress docker container which we can access through a DNS. And our Production environment is going to be an EKS hosted cluster running Wordpress hosted on Kubernetes. We explain why we chose this setup in a later section

Because of the way Wordpress works, the connection between Development and Production comes from a Jenkins job which exports the blog/themes from Development (stores them in an S3) and imports those from the S3 to Production.

### Pipeline Translated to Jenkins Job

The proposed Jenkins job that does the approved Dev website to production could follow the steps below:

1. Once a change is approved, the Jenkins SSH agent SSH's into the Dev Env Instance and runs a set of commands which exports the new XML and then copies that into the S3 bucket (the specific commands are in a file called export.sh in the devenv branch)
2. The Jenkins SSH agent then SSH's into the Supernode (which controls the the wordpress deployment) and runs a script (init.sh) which would build an image which pulls the latest XML from the bucket and also does the wordpress kubernetes redeployment.

### What We Know About How Wordpress Works and What That Means for the Project

#### WP-CLI To Automate Wordpress

To automate Wordpress we use a tool called `wp-cli` which allows for installing, setup and all that through command line. This is a separate tool that we install early in the image building stage. If you go to` /wordpress/docker/Dockerfile` you'll see the codeblock below which installs it. 

```dockerfile
WORKDIR /tmp
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x wp-cli.phar
RUN mv wp-cli.phar /usr/local/bin/wp
```

#### WP-CLI Commands and Options We've Used

##### wp core install
```bash
wp core install --path="/var/www/html" --url="al-website.academy.labs.automationlogic.com" --title="Automatic Logic" --admin_user=admin --admin_password=secret --admin_email=foo@bar.com --allow-root;
```
When you have a fresh Wordpress docker image it has the Wordpress files its working directory `/var/www/html`, this command installs all those files in that directory with the specifics we've given it such as Title, Admin User Name, Admin Email, etc.

We've chosen the URL to be the Route53 DNS we created because that's the way we found it to work. I think it's for the css and html stuff to link correctly, idk ask Jamaine about it.

##### --allow-root 

This flag needs to exist in all wp commands as these commands get run in the pods as root and apparently it's not good practice but what are they gonna do? Shoot me? I doubt it.


##### wp plugin install

This command installs all the plugin dependencies a `.zip` as the argument of the command.

The `--force` flag forces a reinstall the plug-in if it's already installed.

The `--activate` flag activates the plugin after it gets downloaded.
##### wp theme install and wp theme activate

- `wp theme install /tmp/automation-logic.zip`

This installs automation logic `.zip` theme that we put in `/tmp` in the docker build stage. This file can be found at `/wordpress/docker/data/automation-logic.zip`

In the file `/wordpress/docker/Dockerfile` the below line copies all contents (the theme `.zip`, the al blog info in `.xml` configuration files ) in the data file in

```dockerfile
COPY data/* /tmp/
```
##### wp import /tmp/ALProd.xml

This imports the `.xml` which contains AL's blog contents. In `/wordpress/docker/data` we have a "base `.xml`" `automationlogic.WordPress.2021-06-11.xml` which can be chosen if there is any error in the imported xml from dev.

What do I 


#### Importing AL's Wordpress Content, Themes, and Plugins

Wordpres is DUUUUMB. The AL's website is made up of an XML file which includes all the content like blogs, attachments, etc. But for a fully functional AL website we need to also download a lot of plugins and a list of that has been sent to us (link below). This is great but there's also custom AL "post types" which haven't been given to us yet. **But we think that importing the plugins might have fixed that error, we haven't tested that yet**

The importing of all the files into the image happens at the line found in the script `/wordpress/init.sh` in the line
```bash
sudo docker build -t al_wordpress:$VERSION .
```
which builds the image.

The contents being imported include:
- automation-logic.zip
  - The Automation logic theme 
- automationlogic.WordPress.2021-06-11.xml
  - The XML that was sent to us, in the actual pipeline this file will not be called in the `wp import /tmp/automationlogic.WordPress.2021-06-11.xml --authors=create --path="/var/www/html" --allow-root`
- uploads.ini
- wp-config.php

### Setting Up Wordpress

#### Known Problems and How To Reproduce Them


---
##### Missing Plugins

- They haven't given us all the dependencies required to fully run the website. These include custom plugins that the AL website requires. We currently have the theme and the content in XML format, but when we try import the XML we get unable to import errors". Things like attachments and text content do get important correctly because those things are native to Wordpress. AL has created custom "post types"
  - The following are unable to be imported currently:
    - post_type: casestudies
    - post_type: client
    - post_type: insights
    - post_type: search_filter_widget
    - post_type: nav_menu_item
    - post_type: team
    - post_type: table_press_table
    - post_type: news
    - post_type: life_at_al
    - post_type: acf_fields
    - post_type: acf_fields_group
    - post_type: testimonials
    - post_type: service


  - List of plugins we've found so far:
    - WP Video Lightbox
    - Contact form 7
    - wp-pagenavi
    - Page-list
    - Tablepress
    - Search-filter-pro
    - code prettifier 


  - It's possible there is more, we tried our best.
  - We need to ask Orlando to go to the log in to the AL website and export ALL plugins and AL Wordpress Content Type as a zip if possible.
    - If it's not possible then we need to download each plugin through the `wp plugin install https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.6.zip --activate` command.
      - For example this command downloads one of the plugins that wasn't given to us. We found this by Googling the plugin name and finding its `.zip` link 

  - To reproduce this error in the dev environment, do the following:
    1. In the docker-compose file comment out the `$cont_cmd wp import /tmp/ALdev.xml --authors=create --allow-root` line to ensure the import doesn't happen on container creation.
    2. Use the following commands to bash into the container
    ```bash
    container_id=$(docker ps | grep wp | awk '{print $1}')
    docker exec -it $container_id bash
    ```
    3. Run the command `wp import /tmp/ALdev.xml --authors=create --allow-root` manually to see the output and the errors of the content which could not get imported


###### Link to Wordpress Plugins List
[AL Plugins List from Orlando](https://docs.google.com/spreadsheets/d/17QAIka98egT-AbIoyK2R66dknu36m_e5d5I6mqGw--0/edit#gid=0)

- Pluggins be Buggins.
  - We installed plugins based on a google sheets file which was sent to us. This just had the names of the plugins and not links to their download. This meant that 

---
##### When Exporting XML from Dev

- When we created a new blog and tested our export of the new XML from Dev and import to Prod, we found an error in the importing stage.
  - The error said `cannot create post with empty user` and failed to import the entire XML
- We think this is because when you create blogs to, they need to be created with an Author not be kept empty. 
  - We are unsure on how to exactly do this but we think there will be an option in the area where you create a blog, but we don't know yet.

---

### Development Environment
[See documentation here](README_Devenv.md)

### Production Environment
[See documentation here](README_Terraform.md)

## Wordpress Testing CSS/HTML
[See documentation here](README_TeStINg.md)

## Metrics Monitoring and Notifications
[See documentation here](README_Monitoring.md)


## Why we chose the technology we're using

### Why Production on Kubernetes and Development on Docker?

We used Kubernetes because Kubernetes is great the autoscaling and standardising and all the reasons Kubernetes is worth using for.

We used a Docker container to run the dev env as the dev env doesn't need to have as many resources to keep it and we decided to keep the running of the dev env to be as simple as possible.

### Business Considerations

We were asked to provide an monetary estimation on how much it would cost to run this infrastructure in order to compare it with how much AL pay the third party now. You can use this link https://calculator.aws/ to get an estimate of our infrastructure which is:

- 1 RDS for Wordpress
  - t2.micro
- 1 EKS Cluster:
  - Consisting of 3 Worker nodes
- 1 EC2 instance for Dev Env
  - t3.xlarge
- 1 EC2 instance that is our Supernode


## Rollback Deployment for Version Control
Rollback is not an important requirement but with the way our setup is (where every al_wordpress image created has the most latest XML content of wordpress), rollback to the previous deployment (which should also be the previous al_wordpress image) should be straight forward.

Within the Deployment section of the Kubernetes yaml file there is the line `revisionHistoryLimit: 100` within the spec which should enable versioning for the deployments.

```yml
...
...
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: al-wordpress
  name: al-wordpress
  labels:
    app: al-wordpress
spec:
  replicas: 1
  revisionHistoryLimit: 100
...
...
```
Given this rolling back should apparently be done with the second command below. The first command should show the history

```bash
kubectl rollout history deployment/al-wordpress -n al-wordpress
kubectl rollout undo deployment/al-wordpress -n al-wordpress --to-revision=2
```

I don't know if the `revisionHistoryLimit: 100` in the kubernetes yaml has been converted to the kubernetes tf file, but either way we haven't been able to test this command as we haven't gotten a working updated blog (an updated xml) yet.
## Commands Shortcut

```bash
## When you want to bash into a pod, replace the empty space between tty and -- /bin/bash with the pod name that you get from `kubectl get pods -n al-wordpress`
kubectl exec -n al-wordpress --stdin --tty      -- /bin/bash
```


## Things To Do Next

1. Get the full list of plugins/extra dependencies to ensure the whole content is being imported properly.
2. Create a Jenkins job which automates the running of the `export.sh` (in the Dev Instance) found in `wordpress/docker/export.sh` in `devenv` branch. The following command should be running the `init.sh` (in the supernode) found in `wordpress/docker/init.sh` in `production-env` branch